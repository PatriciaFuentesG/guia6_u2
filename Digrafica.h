#ifndef DIGRAFICA_H
#define DIGRAFICA_H

#include <iostream>
#include<cstring>
using namespace std;


class Digrafica {
    private:
     
    public:
        /* constructor*/
        Digrafica();

        void correr_programa(int n, int opc);
        void leer_nodos (char *vector, int n);
        void inicializar_vector_D (int *D, int **M, int n, int origen);
        void inicializar_vector_caracter (char *vector, int n);
        void aplicar_dijkstra (char *V, char *S, char *VS, int *D, int **M, int n,int origen);
        void actualizar_VS(char *V, char *S, char *VS, int n);
        int buscar_indice_caracter(char *V, char caracter, int n);
        int busca_caracter(char c, char *vector, int n);
        void agrega_vertice_a_S(char *S, char vertice, int n);
        int elegir_vertice(char *VS, int *D, char *V, int n, int origen);
        void actualizar_pesos (int *D, char *VS, int **M, char *V, char v, int n);
        int calcular_minimo(int dw, int dv, int mvw);
        void imprimir_vector_caracter(char *vector, char *, int n);
        void imprimir_vector_entero(int *vector, int n);
        void imprimir_matriz(int **matriz,int n);
        void imprimir_grafo(int **matriz, char *vector, int n);
        void inicializar_matriz_enteros (int **matriz, int n);      
        
};
#endif
