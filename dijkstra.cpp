#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include "Digrafica.h"
using namespace std;


int menu(){
  int opc;
  cout <<"**********Menu***********"<<endl;
  cout <<"Aplicar Dijkstra______[1]"<<endl;
  cout <<"Imprimir grafo________[2]"<<endl;
  cout <<"Salir_________________[0]"<<endl;
  cout <<"*************************"<<endl;
  cout <<"Seleccione: ";
  cin >> opc;
  cout << endl;
  
 
 return opc;
}

int main(int argc, char **argv) {
 int opc = -1;
 if (atoi(argv[1])<=1){
        cout << "Uso: \n./matriz n" << endl;
        return -1;
  }else{
  Digrafica *digrafica = new Digrafica;

  while(opc != 0){
   opc = menu();
   if(opc ==1){
      digrafica -> correr_programa(atoi(argv[1]),1);
   }else if(opc == 2){
       digrafica -> correr_programa(atoi(argv[1]),2);
   }else{
     return -1;
   }
 }

  delete digrafica;}

  return 0;
}

