#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include "Digrafica.h"
using namespace std;
#define TRUE 0
#define FALSE 1


Digrafica::Digrafica() {}
//Cada vez que se corre esta funcion se crea una nueva matriz tamaño n*n
void Digrafica::correr_programa(int n, int opc){
  char V[n], S[n], VS[n];
  int D[n];
  int i = 0;
  int o = 0;
  int origen;
  int **matriz;
  matriz = new int*[n];
 
  for(int i=0; i<n; i++)
        matriz[i] = new int[n];

    inicializar_matriz_enteros(matriz, n);

  inicializar_vector_caracter(V,n);
  inicializar_vector_caracter(S,n);
  inicializar_vector_caracter(VS,n);

 
  if(opc ==1){
      cout<<"Escoger punto de origen:"<<endl;
      cin>> origen;
      leer_nodos(V,n);
      aplicar_dijkstra (V, S, VS, D, matriz,n,origen);
      imprimir_grafo(matriz, V,n);
  }else if(opc == 2){
      leer_nodos(V,n);
      imprimir_grafo(matriz, V,n);
     }
 
}



// copia contenido inicial a D[] desde la matriz M[][].
void Digrafica::inicializar_vector_D (int *D, int **M, int n,int origen) {
  int col;
  
  for (col=0; col<n; col++) {
    D[col] = M[origen][col];
  }
}

// inicializa con espacios el arreglo de caracteres.
void Digrafica::inicializar_vector_caracter (char *vector, int n) {
  int col;
  
  for (col=0; col<n; col++) {
    vector[col] = ' ';
  }
}

void Digrafica::aplicar_dijkstra (char *V, char *S, char *VS, int *D, int **M, int n, int origen) {
  int i;
  int v;
  char s[]= "S";
  char vs[]= "VS";
  
  // inicializar vector D[] segun datos de la matriz M[][] 
  // estado inicial.
  inicializar_vector_D(D,M,n,origen);

  //
  cout<<"---------Estados iniciales ---------------------------------------\n";
  imprimir_matriz(M,n);
  cout<<endl;
  imprimir_vector_caracter(S, s,n);
  imprimir_vector_caracter(VS, vs,n);
  imprimir_vector_entero(D,n);
  cout<<"------------------------------------------------------------------\n\n";

  // agrega primer véctice.
  cout<<"> agrega primer valor V["<<origen<<"] a S[] y actualiza VS[]\n\n";
  agrega_vertice_a_S (S, V[origen],n);
  imprimir_vector_caracter(S, s,n);
  //
  actualizar_VS (V, S, VS,n);
  imprimir_vector_caracter(VS, vs,n);
  imprimir_vector_entero(D,n);

  //
  for (i=1; i<n; i++) {
    // elige un vértice en v de VS[] tal que D[v] sea el mínimo 
    cout<<"\n> elige vertice menor en VS[] según valores en D[]\n";
    cout<<"> lo agrega a S[] y actualiza VS[]\n";
    v = elegir_vertice (VS, D, V, n,origen);

    //
    agrega_vertice_a_S (S, v,n);
    imprimir_vector_caracter(S, s,n);

    //
    actualizar_VS (V, S, VS,n);
    imprimir_vector_caracter(VS, vs,n);

    //
    actualizar_pesos(D, VS, M, V, v,n);
    imprimir_vector_entero(D,n);
  }
 

}


void Digrafica::inicializar_matriz_enteros (int **matriz, int n) {
    for (int fila=0; fila<n; fila++) {
        for (int col=0; col<n; col++) {
            int costo;
            
             if(fila == col){
                costo = 0;

            }else{
                cout << "Ingrese la distancia entre "<< "["<<fila<<","<<col<<"] ="<<endl;
                cin >> costo;
            }
            matriz[fila][col] = costo;
        }
    }
}


//
void Digrafica::actualizar_pesos (int *D, char *VS, int **M, char *V, char v, int n) {
  int i = 0;
  int indice_w, indice_v;

  cout<<"\n> actualiza pesos en D[]\n";
  
  indice_v = buscar_indice_caracter(V, v, n);
  while (VS[i] != ' ') {
    if (VS[i] != v) {
      indice_w = buscar_indice_caracter(V, VS[i],n);
      D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], M[indice_v][indice_w]);
    }
    i++;
  }
}

int Digrafica::calcular_minimo(int dw, int dv, int mvw) {
  int min = 0;

  //
  if (dw == -1) {
    if (dv != -1 && mvw != -1)
      min = dv + mvw;
    else
      min = -1;

  } else {
    if (dv != -1 && mvw != -1) {
      if (dw <= (dv + mvw))
        min = dw;
      else
        min = (dv + mvw);
    }
    else
      min = dw;
  }
  
  cout<<"dw: "<< dw<<" dv: "<< dv<< " mvw: "<< mvw <<" min: "<< min<<"\n";

  return min;
}

void Digrafica::agrega_vertice_a_S(char *S, char vertice, int n) {
  int i;
  
  for (i=0; i<n; i++) {
    if (S[i] == ' ') {
      S[i] = vertice;
      return;
    }
  }  
}


// elige vértice con menor peso en VS[].
// busca su peso en D[].
int Digrafica::elegir_vertice(char *VS, int *D, char *V, int n,int origen) {
  int i = 0;
  int menor = 0;
  int peso;
  int vertice;
  int cant_VS = 0;

  while (VS[i] != ' ') {
    cant_VS = cant_VS+1;
    peso = D[buscar_indice_caracter(V, VS[i],n)];
    // descarta valores infinitos (-1) y 0.
    if ((peso != -1) && (peso != 0)) {
      if (i == origen) {
        menor = peso;
        vertice = VS[i];
      } else {
        if (peso < menor) {
          menor = peso;
          vertice = VS[i];
        }if(cant_VS == 1){
          vertice = VS[i];
        }
      }
    }

    i++;
  }
 

  printf("\nvertice: %c\n\n", vertice);
  return vertice;
}


int Digrafica::buscar_indice_caracter(char *V, char caracter, int n) {
  int i;
  
  for (i=0; i<n; i++) {
    if (V[i] == caracter)
      return i;
  }
  
  return i;
}

// busca la aparición de un caracter en un vector de caracteres.
int Digrafica::busca_caracter(char c, char *vector,int n) {
  int j;
  
  for (j=0; j<n; j++) {
    if (c == vector[j]) {
      return TRUE;
    }
  }
  
  return FALSE;
}

// actualiza VS[] cada ves que se agrega un elemnto a S[].
void Digrafica::actualizar_VS(char *V, char *S, char *VS, int n) {
  int j;
  int k = 0;
  
  inicializar_vector_caracter(VS,n);
  
  for (j=0; j<n; j++){
    // por cada caracter de V[] evalua si está en S[],
    // Sino está, lo agrega a VS[].
    if (busca_caracter(V[j], S,n) != TRUE) {
      VS[k] = V[j];
      k++;
    }
  }
}

// lee datos de los nodos.
// inicializa utilizando código ASCII.
void Digrafica:: leer_nodos (char *vector, int n) {
  int i;
  int inicio = 97;
  
  for (i=0; i<n; i++) {
    vector[i] = inicio+i;
  }
}

// imprime el contenido de un vector de caracteres.
void Digrafica::imprimir_vector_caracter(char *vector, char *nomVector, int n) {
  int i;
  
  for (i=0; i<n; i++) {
    cout<<nomVector<< "["<<i<<"]: "<<vector[i]<<" ";
  }
  
  cout<<"\n";
}

//
void Digrafica::imprimir_vector_entero(int *vector, int n) {
  int i;
  
  for (i=0; i<n; i++) {
    cout<<"D["<<i<<"]: "<<vector[i]<<" ";
  }
  
  cout<<"\n";
}

// imprime el contenido de una matriz bidimensional de enteros.
void Digrafica::imprimir_matriz(int **matriz, int n) {
  int i, j;
  
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      cout<<"matriz["<<i<<","<<j<<"]: "<<matriz[i][j]<<" ";
    }
    cout<<"\n";
  }
}

// genera y muestra apartir de una matriz bidimensional de enteros
// el grafo correspondiente.
void Digrafica::imprimir_grafo(int **matriz, char *vector, int n) {
  int i, j;
  ofstream fp;
  
  fp.open("grafo.txt");

  fp << "digraph G {" << endl;
  fp << "graph [rankdir=LR]"<<endl;
  fp << "node [style=filled fillcolor=olivedrab1];"<<endl;
  
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++) {
      // evalua la diagonal principal.
      if (i != j) {
        if (matriz[i][j] > 0) {
          fp <<vector[i]<<"->"<<vector[j]<<"[label="<<matriz[i][j]<<"];"<<endl;
         
        }
      }
    }
  }
  
  fp<<"}"<<endl;
  fp.close();

  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
}

